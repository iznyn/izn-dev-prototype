(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function($)
{
  "use strict";

  $(document).ready(function()
  {

    //Cover to
    $( '.book--page--cover, .book--cover--trigger' ).click( function()
    {
      $( '.book' ).turn( 'next' );
      return false;
    });

    //Auto open book
    setTimeout( function()
    {
      $( '.book' ).turn( 'next' );
    }, 1000);

    //Setup book
    $( '.book--wrapper' ).attr( 'data-cover', '1' );
    $( '.book--wrapper' ).attr( 'data-page', '1' );
    $( '.book--wrapper' ).attr( 'data-first-layer', '0' );

    $( '.book' ).turn({
      gradients: true,
      acceleration: true
    });

    //Fired before page turning
    $( ".book" ).bind("turning", function(event, page, view)
    {
      var currentPage = $( '.book--wrapper').attr( 'data-page' );

      if ( $( '.book--wrapper').hasClass( '_turn--start' ) ) {
        event.preventDefault();
      }
      $( '.book--wrapper' ).removeClass( '_turn--end' );
      $( '.book--wrapper' ).addClass( '_turn--start' );
      $( '.book--wrapper' ).attr( 'data-page-before', page );

      if ( currentPage == 1 ) {
        $( '.book--wrapper' ).attr( 'data-from-cover', '1' );
      } else {
        $( '.book--wrapper' ).attr( 'data-from-cover', '0' );
      }

      //Change book size
      if ( page == 1 ) {
        $( '.book' ).turn( 'size', 832, 531 );
      }

      //Show/hide text intro
      if ( page == 2 ) {
        $( '.book--text--intro' ).addClass( '_hide' );
      }
      else if ( page == 1 ) {
        $( '.book--text--intro' ).removeClass( '_hide' );
      }

      if ( page > 1 )
      {
        setTimeout(function()
        {
          $( '.book--wrapper' ).attr( 'data-shadow-cover', '0' );
        },400);
      }
      else {
        $( '.book--wrapper' ).attr( 'data-shadow-cover', '1' );
      }
    });

    //Fired after page turn
    $( ".book" ).bind("turned", function(event, page, view)
    {
      $( '.book--wrapper' ).addClass( '_turn--end' );
      $( '.book--wrapper' ).removeClass( '_turn--start' );
      $( '.book--wrapper' ).removeAttr( 'data-page-before', page );
      $( '.book--wrapper' ).attr( 'data-page', page );

      if ( page == 1 ) {
        $( '.book--wrapper' ).attr( 'data-cover', '1' );
      } else {
        $( '.book--wrapper' ).attr( 'data-cover', '0' );
      }

      if ( page == 2 || page == 3 ) {
        $( '.book--wrapper' ).attr( 'data-first-layer', '1' );
      } else {
        $( '.book--wrapper' ).attr( 'data-first-layer', '0' );
      }

      //Change book size
      if ( page == 2 ) {
        $( '.book' ).turn( 'size', 684, 510 );
      }

      //Hide/show cover trigger
      if ( page > 1 ) {
        $( '.book--cover--trigger' ).hide();
      } else {
        $( '.book--cover--trigger' ).show();
      }

      //
      //Signup form validation
      if ( page >= 14 )
      {
        //
        //Form signup validate
        //
        $( '#form--signup' ).validate(
        {
          errorClass: "form--input--error--msg",
          errorElement: "div",
          errorPlacement: function( error, element )
          {
            var input = element.parents( '.form--input' );
            var elerror = $( '<div class="form--input--error"></div>' );
            elerror.append( error );
            input.append( elerror );
          },
          highlight: function( element, errorClass, validClass )
          {
            $(element)
              .parents( '.form--field' )
              .addClass( 'form--invalid' );
          },
          unhighlight: function(element, errorClass, validClass)
          {
            $(element)
              .parents( '.form--field' )
              .removeClass( 'form--invalid' );

          },
          onkeyup: false,
          rules: {
            title: {
              required: false
            },
            alias: {
              required: false
            },
            category: {
              required: true
            },
            sub_category: {
              required: false
            },
            filename: {
              required: true
            },
            file_play: {
              required: false
            },
            file_preview: {
              required: false
            },
            icon: {
              required: false
            },
            icon1: {
              required: false
            },
            icon2: {
              required: false
            },
            version: {
              required: false
            },
          },
          messages: {},
          submitHandler: function(form)
          {
            console.log( 'processed' );
          }
        });
      }
    });

    //
    //Next/Prev Nav
    $( '.book--page--next' ).click( function(e)
    {
      $( '.book' ).turn( 'next' );
      e.preventDefault();
    });
    $( '.book--page--prev' ).click( function(e)
    {
      $( '.book' ).turn( 'previous' );
      e.preventDefault();
    });

    //
    //Site Nav
    $( '._menu--legend a' ).click( function()
    {
      $( '.book' ).turn( 'page', 2 );
      return false;
    });

    $( '._menu--council a' ).click( function()
    {
      $( '.book' ).turn( 'page', 8 );
      return false;
    });

    $( '._menu--announcement a' ).click( function()
    {
      $( '.book' ).turn( 'page', 12 );
      return false;
    });

    $( '._menu--signup a' ).click( function()
    {
      $( '.book' ).turn( 'page', 14 );
      return false;
    });

    // function centeringVerticaly()
    // {
    //   var oriWidth     = 1366;
    //   var oriHeight    = 768;
    //   var oriRowHeight = 232;
    //   var oriTop       = 211;
    //   var winHeight    = $(window).height();
    //   var winWidth     = $(window).width();
    //   var sizeRatio    = winWidth/winHeight;
    //   var contHeight   = $( '.splash--main--inner' ).height();
    //
    //   if ( sizeRatio >= 1.6 )
    //   {
    //     var currTop = winHeight/oriHeight * oriTop;
    //     $( '.splash--main--inner' ).css( 'paddingTop', currTop + 'px' );
    //
    //     var paddTop = parseInt( $( '.splash--main--inner' ).css( 'paddingTop' ) );
    //
    //     if ( (paddTop+contHeight) > winHeight )
    //     {
    //       var toTop = (winHeight - contHeight)/2;
    //       $( '.splash--main--inner' ).css( 'paddingTop', toTop + 'px' );
    //     }
    //   }
    //   else
    //   {
    //     if ( contHeight < winHeight )
    //     {
    //       var toTop = (winHeight - contHeight)/2;
    //       $( '.splash--main--inner' ).css( 'paddingTop', toTop + 'px' );
    //     }
    //     else {
    //       $( '.splash--main--inner' ).css( 'paddingTop', '20px' );
    //     }
    //   }
    // }
    //
    // if ( $( '.splash--main--inner' ).length > 0 )
    // {
    //   centeringVerticaly();
    //
    //   $(window).resize(function()
    //   {
    //     centeringVerticaly();
    //   });
    // }
    //
    // //
    // //Book centering
    // //
    // function centeringBook()
    // {
    //   var headerHeight = $( '.header' ).height();
    //   var footerHeight = $( '.footer' ).height();
    //   var windowHeight = $(window).height();
    //
    //   var maxMainHeight = 632;
    //   var maxMainWidth  = 895;
    //   var mainHeight    = windowHeight - headerHeight - footerHeight - 30;
    //
    //   if ( mainHeight < maxMainHeight && mainHeight > 300 )
    //   {
    //     $( '.main' ).height( mainHeight );
    //
    //     //get scale
    //     var mainWidth = maxMainWidth/maxMainHeight * mainHeight;
    //
    //     //set inner size
    //     var oriInnerWidth    = 692;
    //     var oriInnerHeight   = 530;
    //     var innerWidth       = mainWidth/maxMainWidth*oriInnerWidth;
    //     var innerHeight      = mainHeight/maxMainHeight*oriInnerHeight;
    //
    //     //Set position
    //     var oriTop    = 19;
    //     var oriLeft   = 122;
    //     var innerTop  = mainHeight/maxMainHeight*oriTop;
    //     var innerLeft = mainWidth/maxMainWidth*oriLeft;
    //
    //     $( '.main--inner' )
    //     .css( 'paddingTop', innerTop+'px' )
    //     .css( 'paddingLeft', innerLeft+'px' );
    //
    //     $( '.main--content' )
    //       .innerHeight( innerHeight )
    //       .innerWidth( innerWidth );
    //   }
    // }
    //
    // if ( $( '.site' ).length > 0 )
    // {
    //   centeringBook();
    //
    //   $(window).resize(function()
    //   {
    //     centeringBook();
    //   });
    // }

  });

})(jQuery);

},{}],2:[function(require,module,exports){
(function($) {
  "use strict";

  $(document).ready(function()
  {

  });
})(jQuery);

},{}]},{},[1,2]);
